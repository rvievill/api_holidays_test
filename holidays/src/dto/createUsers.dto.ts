export class CreateUsersDTO {
    readonly name: String;
    readonly date: [{
        readonly begin: Number;
        readonly end: Number
    }]
}