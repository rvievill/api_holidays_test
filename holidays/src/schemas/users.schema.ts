import * as mongoose from 'mongoose';

export const UsersSchema = new mongoose.Schema({
    name: String,
    date: [{
        begin: Number,
        end: Number
   }]
});