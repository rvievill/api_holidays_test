import { Controller, Get, Post, Put, Body } from '@nestjs/common';
import { GetService } from '../services/get.service'
import { PostService } from 'src/services/post.service';
import { PutService } from 'src/services/put.service';


@Controller('holidays')
export class HolidaysController {

    constructor(
        private readonly getService: GetService,
        private readonly postService: PostService,
        private readonly poutService: PutService
    ) {}

    @Get()
    viewHolidays(@Body() body) : any {
        return this.getService.viewHolidays(body.name);
    }

    @Post()
    createBooking(@Body() body) : any {
        return (this.postService.createHolidays(body));
    }

    @Put()
    editHolidays(@Body() body) : any {
        return (this.poutService.editHolidays(body))

    }
}
