import { Document } from 'mongoose';

export interface Users extends Document {
    readonly name: String;
    readonly date: [{
        readonly begin: Number,
        readonly end: Number
    }]
}