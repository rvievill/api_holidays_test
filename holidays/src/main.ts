import { NestFactory } from '@nestjs/core';
import { HolidaysModule } from './modules/holidays.module';

async function bootstrap() {
  const app = await NestFactory.create(HolidaysModule);
  await app.listen(3000);
}
bootstrap();
