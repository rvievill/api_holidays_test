import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'
import { HolidaysController } from '../controllers/holidays.controller';
import { PutService } from '../services/put.service';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { UsersService } from '../services/users.service';
import { CheckService } from '../services/check.service';
import { UsersSchema } from '../schemas/users.schema';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27018/'),
    MongooseModule.forFeature([{ name: 'Users', schema: UsersSchema }])
  ],
  controllers: [HolidaysController],
  providers: [
    PutService,
    GetService,
    PostService,
    UsersService,
    CheckService
  ],
})
export class HolidaysModule {}
