import { Injectable, Body } from '@nestjs/common';
import { UsersService } from './users.service'


@Injectable()
export class CheckService {
    curDate : Number;
    constructor(private readonly userService : UsersService) {
        this.curDate = new Date().getTime();
    }

    CheckValideDate(begin: number, end: number) : Promise<Boolean> {
        return new Promise((resolve, reject) => {
            if (begin > end){
                reject({code: 400, msg: "The begin is after the end !"})
            } else {
                resolve();
            }
        });
    }

    CheckPast(begin: number, end: number) : Promise<Boolean> {
        return new Promise((resolve, reject) => {
            if (begin < this.curDate){
                reject({code: 400, msg: "The holidays is in past"});
            } else {
                resolve();
            }
        });
    }

    CheckDuration(begin: number, end: number) : Promise<Boolean> {
        return new Promise((resolve, reject) => {
            const delta : Date = new Date(end - begin);             // delta between begin and end holiday
            const nbDay : number = (end - begin) / (86400000)       // (3600*24*1000) number of days between begin and end holiday
            const start : number = delta.getDay();                  // number of day of week
            const nbWeek : number = Math.floor((start + nbDay) / 7) // number of weekend in holiday
            const businessDay = nbDay - (2 * nbWeek);               // number of business day

            if (businessDay > 15) {
                reject({code: 400, msg: "the duration of holidays is higher 15 business days"});
            } else {
                resolve();
            }
        });
    }

    CheckOverlap(name: string, begin: number, end: number) : Promise<Boolean> {
        return new Promise((resolve, reject) => {
            this.userService.getUserHolidayDate(name)
                .then(res => {
                    const user : any = res;
                    if (res !== null)
                    {
                        for (let i in res.date) {
                            if ((begin >= res.date[i].begin && begin <= res.date[i].end) ||
                                (end >= res.date[i].begin && end <= res.date[i].end))
                            {
                                reject({code: 400, msg: "The holidays overlap others holidays"});
                            }
                        }
                    }
                    resolve();
                }).catch((err) => {
                    reject({code: 400, msg: "The holidays overlap others holidays"});
                });
        });
    }
}
