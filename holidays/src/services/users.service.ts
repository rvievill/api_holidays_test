import { Injectable, Body } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Users } from '../interfaces/Users.interface';
import { CreateUsersDTO } from '../dto/createUsers.dto';


@Injectable()
export class UsersService {

    constructor(@InjectModel('Users') private readonly UsersModel: Model<Users>) {}

    getUserHolidayDate(name : string) : Promise<any> {
        return new Promise((resolve, reject) => {
            this.UsersModel.findOne({name})
                .then(resolve)
                .catch(reject);
        });
    }

    addHolidays(@Body() body) : void {
        let data = { 
            date : [{
                begin : new Date(body.begin).getTime(),
                end: new Date(body.end).getTime()
            }]
        };
        this.UsersModel.find({name : body.name}).then((res) => {
            if (res.length === 0) {
                data['name'] = body.name;
            }
        })
        this.UsersModel.updateOne({ name : body.name }, { $push : data }, { upsert: true }, (err, res) => {
            if (err){
                console.error(err);
            }
            console.log("UPDATE RESPONSE: ", res);
        })
    }

    editHolidays(@Body() body) : void {
        const dataSearch = {
            name : body.name,
            date : [{
                begin : new Date(body.begin).getTime(),
                end: new Date(body.end).getTime()
            }]
        }
        let dataUp = { 
            date : [{
                begin : new Date(body.begin).getTime(),
                end: new Date(body.end).getTime()
            }]
        };
        this.UsersModel.updateOne({ dataSearch }, { $set : dataUp }, { upsert: false }, (err, res) => {
            if (err){
                console.error(err);
            }
            console.log("UPDATE RESPONSE: ", res);
        })
    }

    viewHolidays(name: string) : Promise<any> {
        return new Promise((resolve, reject) => {
            this.UsersModel.findOne({name})
                .then(resolve)
                .catch(reject);
        });
    }
}