import { Injectable, Body } from '@nestjs/common';
import { UsersService } from './users.service';

@Injectable()
export class GetService {
    
    constructor(private readonly userService : UsersService) {}

    viewHolidays(name: string) : any {
        return (this.userService.viewHolidays(name));
    }
}
