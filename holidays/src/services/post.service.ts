import { Injectable, Body } from '@nestjs/common';
import { UsersService } from './users.service'
import { CheckService } from './check.service'


@Injectable()
export class PostService {
    curDate : Number;
    constructor(
            private readonly userService : UsersService,
            private readonly checkService : CheckService
        ) {
        this.curDate = new Date().getTime();
    }

    createHolidays(@Body() body) : any {
        const begin : Date = new Date(body.begin);
        const end : Date = new Date(body.end);
        const beginSecond : number = begin.getTime();
        const endSecond : number = end.getTime();

        Promise.all([
            this.checkService.CheckValideDate(beginSecond, endSecond),
            this.checkService.CheckPast(beginSecond, endSecond),
            this.checkService.CheckDuration(beginSecond, endSecond),
            this.checkService.CheckOverlap(body.name, beginSecond, endSecond)
        ]).then(respunce => {
            this.userService.addHolidays(body);
            return ({code: 200, msg: "The holidays were add"})
        }).catch(err => {
            console.log(err);
        });
    }

}  
